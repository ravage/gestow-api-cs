using System.Data;
using System.Threading.Tasks;
using Dapper;
using Gestow.API.Entities;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Gestow.Repositories
{
    public class GPSRepository: BaseRepository
    {
        public GPSRepository(IConfiguration configuration) : base(configuration) {}

        public Task<int> Insert(GPS data) {
            return Execute(db => db.ExecuteAsync(Configuration.GetSection("queries")["insert"], data));
        }
    }
}