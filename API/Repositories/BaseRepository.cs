using System;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Gestow.Repositories {
    public abstract class BaseRepository
    {
        public BaseRepository(IConfiguration configuration) {
            Configuration = configuration;
        }

        protected async Task<int> Execute(Func<IDbConnection, Task<int>> query) {
            using (IDbConnection db = new NpgsqlConnection(Configuration.GetConnectionString("pg"))) {
                return await query.Invoke(db);
            }
        }

        public IConfiguration Configuration { get; }
    }
}