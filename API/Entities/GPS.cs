namespace Gestow.API.Entities
{
    public class GPS
    {
        public int Id { get; set; }
        public string TowRegistry { get; set; }
        public string CMD { get; set; }
        public string SerialNumber { get; set; }
        public long DateTimeInt { get; set; }
        public int Validity { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string HemisphereLat { get; set; }
        public string HemisphereLong { get; set; }
        public int SatNumber { get; set; }
        public float Speed { get; set; }
        public int Direction { get; set; }
        public int Baterie { get; set; }
        public int GSM { get; set; }
        public int Roaming { get; set; }
        public int Acc_Ignition { get; set; }
        public int Block { get; set; }
        public int input_1 { get; set; }
        public int input_2 { get; set; }
        public int input_3 { get; set; }
        public int input_4 { get; set; }
        public string terminal { get; set; }
    }
}