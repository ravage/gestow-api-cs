using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Dynamic;
using Gestow.API.Entities;
using System.Data;
using Npgsql;
using Dapper;
using Microsoft.Extensions.Configuration;
using Gestow.Repositories;

namespace Gestow.API
{
    [Route("[controller]")]
    public class GPSController : Controller
    {
        public GPSController(IConfiguration configuration) {
            Configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> Store(GPS data) {
            await new GPSRepository(Configuration).Insert(data);
            return Ok();
        }

        public IConfiguration Configuration { get; }

                /*
        [HttpGet]
        public IActionResult Index()
        {
            return Ok();
        }

        [HttpGet("{id}")]
        public string Show(int id)
        {
            return "show";
        }

        [HttpGet("new")]
        public string Create() { return ""; }

        [HttpGet("{id}/edit")]
        public string Edit(int id) { return ""; }

        [HttpPut("{id}")]
        [HttpPatch("{id}")]
        public void Update(int id) { }

        [HttpDelete("{id}")]
        public void Destroy(int id) { }
        */
    }
}