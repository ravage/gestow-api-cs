CREATE TABLE gestow (
    id SERIAL PRIMARY KEY,
    towRegistry VARCHAR(255),
    cmd VARCHAR(255),
    serialNumber VARCHAR(255),
    dateTimeInt INTEGER,
    validity INTEGER,
    latitude REAL,
    longitude REAL,
    hemisphereLat VARCHAR(255),
    hemisphereLong VARCHAR(255),
    satNumber INTEGER,
    speed REAL,
    direction INTEGER,
    baterie INTEGER,
    gsm INTEGER,
    roaming INTEGER,
    acc_ignition INTEGER,
    block INTEGER,
    input_1 INTEGER,
    input_2 INTEGER,
    input_3 INTEGER,
    input_4 INTEGER,
    terminal VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)
